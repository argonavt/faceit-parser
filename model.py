import pymysql


class DataBase:
    """CRUD fro DB"""

    def __init__(self):
        self.conn = pymysql.connect(
            host='localhost',
            user='parser',
            password='*******',
            db='parser',
            cursorclass=pymysql.cursors.DictCursor,
            unix_socket='/var/lib/mysql/mysql.sock'
        )
        self.cursor = self.conn.cursor()

    def __del__(self):
        self.conn.close()


class UserListModel(DataBase):
    """CRUD for UserList crawler"""

    def __init__(self):
        super().__init__()

    def __del__(self):
        super().__del__()

    def insert_user(self, name, processed=0):
        res = True
        try:
            self.cursor.execute('INSERT INTO parser_queue (user_name, processed) VALUES (%s, %s)', (name, processed,))
        except pymysql.IntegrityError:
            res = False
        finally:
            self.conn.commit()
            return res


class UsersModel(DataBase):
    """CRUD for Users crawler"""

    def __init__(self):
        super().__init__()

    def __del__(self):
        super().__del__()

    def get_unprocessed_users(self):
        """Get unprocessed users from user list"""
        while True:
            self.cursor.execute('SELECT user_name FROM parser_queue WHERE processed = %s ORDER BY RAND() LIMIT 1', (0,))
            res = self.cursor.fetchone()
            if res:
                # set queue row as processed
                self.cursor.execute("UPDATE parser_queue SET processed = %s WHERE user_name = %s", (1, res['user_name'],))
                self.conn.commit()
                yield res['user_name']
            else:
                break

    def save_new_user(self, data: dict) -> bool:
        """Save new user to 'player' table """
        result = True
        qmarks = ', '.join(['"%s"'] * len(data))
        keys = ', '.join(data.keys())
        query = "INSERT INTO player ({:s}) VALUES ({:s})".format(keys, qmarks)
        try:
            self.cursor.execute(query, list(data.values()))
        except pymysql.IntegrityError:
            result = False
        finally:
            self.conn.commit()
            return result

    def save_friends(self, user_id: str, friends: list):
        """Add friends to db"""
        query = "INSERT INTO friends (player_id, friend_id) VALUES (%s, %s)"

        for friend in friends:
            self.cursor.execute(query, (user_id, friend,))
        self.conn.commit()


class MatchModel(DataBase):
    """CRUD for Match crawler"""

    def __init__(self):
        super().__init__()

    def __del__(self):
        super().__del__()

    def save_match_info(self, matches: list):
        """Save user matches info like: match id,team,Player,Kills,Assists ..."""

        qmarks = ', '.join(['%s'] * len(matches[0]))
        keys = ', '.join(matches[0].keys())
        query = "INSERT INTO match_information ({:s}) VALUES ({:s})".format(keys, qmarks)

        for match in matches:
            self.cursor.execute(query, list(match.values()))
        self.conn.commit()

    def dave_match_history(self, match: dict):
        """Save user match history like: player id, matchid, match datetime, mode, result ..."""

        qmarks = ', '.join(['%s'] * len(match))
        keys = ', '.join(match.keys())
        query = "INSERT INTO match_history ({:s}) VALUES ({:s})".format(keys, qmarks)

        self.cursor.execute(query, list(match.values()))
        self.conn.commit()
