#!/var/www/test.com/env/bin/python3

import crawler
import threading
import config


if __name__ == '__main__':

    # run crawlers for getting list of all users
    user_list = crawler.UserList()
    user_list.run()

    # run crawler for each user from user list
    for i in range(config.threades):
        user = crawler.Users()
        threading.Thread(target=user.run).start()
