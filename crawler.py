from driver import Driver
from abc import ABCMeta, abstractmethod
import xml.etree.ElementTree as et
import requests
import time
import model
import config


def show_execution_time(func):
    """Decorator for showing script execution time"""
    def wrapper(self, **kwargs):
        start_time = time.time()

        func(self, **kwargs)

        m, s = divmod(time.time() - start_time, 60)
        h, m = divmod(m, 60)
        print("Elapsed time: {:0>2}:{:0>2}:{:0>2}".format(int(h), int(m), round(s, 1)))

    return wrapper


def show_execution_info(name):
    """Decorator for showing script execution info"""
    def decorator(func):
        def wrapper(self, **kwargs):
            print("Start {}".format(name))
            func(self, **kwargs)
            print("End {}".format(name))
        return wrapper
    return decorator


class Crawler(metaclass=ABCMeta):
    """Get data from site"""

    def __init__(self):
        self._model = model.UserListModel()

    @abstractmethod
    def run(self):
        pass

    @staticmethod
    def get_api_json(url: str) ->dict:
        """Get json data and convert it to dict"""
        source = requests.get(url)
        return source.json()


class UserList(Crawler):
    """Get all users from rank pages"""

    def __init__(self):
        super().__init__()
        self._driver = Driver()
        self._driver.load_page('https://www.faceit.com/en/csgo')

    @show_execution_time
    @show_execution_info('User list scraper')
    def run(self):
        """Run link collector and put users to db"""

        users_added = 0
        for rank in range(1, self._get_rank_count()+1):

            # # get ladder from phantomjs
            # rank_link = ("https://www.faceit.com/en/csgo/league/{}/{}?rank={}"
            #              .format(config.region, config.league_type, rank))
            # for ladder in self.get_phantom_ladders(rank_link):

            # get ladder from api (faster way)
            for ladder in self.get_api_ladder(rank):
                # get users
                for user in UserList.get_users(ladder):
                    # add user to db queue
                    res = self._model.insert_user(user)
                    if res:
                        users_added += 1
        print('{:>4}{:d} users was added'.format('', users_added))

    def _get_rank_count(self) -> int:
        """Get count of ranks"""
        return len(self._driver.select_nodes('.league-cta__list__item'))

    @staticmethod
    def get_users(ladder: str):
        """Get users from api"""
        url = "https://api.faceit.com/api/leagues/{}/ladders/{}?rank=100000".format(config.api_key, ladder)
        data = Crawler.get_api_json(url)

        for user in data['payload']['sorted_rankings']:
            yield user['nickname']

    def get_phantom_ladders(self, link):
        """Run through all `ladder select` form and yield ladder key"""

        self._driver.load_page(link)

        for i in range(len(self._driver.select_nodes('.league-detail__filters__item select option'))):
            option = self._driver.select_nodes('.league-detail__filters__item select option')[i]
            option.click()
            self._driver.implicitly_wait(1)
            yield self._driver.current_url.split('&')[-1].replace('ladder=', '')

    def get_api_ladder(self, rank: int) -> str:
        """Get ladder from api"""

        # get league ID
        url = 'https://api.faceit.com/api/leagues?game=csgo&match_type={:s}&rank={:d}&region={:s}'
        legue_data = Crawler.get_api_json(url.format(config.league_type, rank, config.region))
        legue_id = legue_data['payload'][0]['guid']

        # get ladder ID
        ladder_data = Crawler.get_api_json('https://api.faceit.com/api/leagues/{}/ladders'.format(legue_id))
        for ladder in ladder_data['payload']:
            yield ladder['guid']


class Users(Crawler):
    """Get all data about user"""

    def __init__(self):
        super().__init__()
        self._model = model.UsersModel()

    @show_execution_time
    @show_execution_info('User crawler')
    def run(self):
        """Run crawler trough user list and parse data"""

        i = 1
        for user in self._model.get_unprocessed_users():
            # get and save user data
            user_info = Users.get_info(user)
            res = self.prepare_info(user_info)

            if not res:
                continue

            # parse user matches
            matches = Match()
            matches.parse_matches_information(user_info['payload']['guid'])
            matches.run()

            # add match users to parse queue
            matches.set_users()

            # print('{:<4}Counter - {:d}'.format('', i))
            i += 1
            if config.thread_parse_limit and i > config.thread_parse_limit:
                break

    def prepare_info(self, user_info: dict) -> bool:
        """Prepare user info for saving to db"""

        # Except iff user is not found
        try:
            league_info = Users.get_league_info(user_info['payload']['guid'])
        except KeyError:
            return False

        # Except steam api errors
        try:
            steam_hours = Users.get_steam_hours(user_info['payload']['steam_id_64'])
        except (et.ParseError, KeyError):
            steam_hours = ''

        # Except for API errors
        try:
            user_rank = league_info['payload']['leagues'][0]['ladder']['user_rank']['rank']
            user_points = league_info['payload']['leagues'][0]['ladder']['user_rank']['points']
            user_name = league_info['payload']['leagues'][0]['rank_label']
        except KeyError:
            user_rank = ''
            user_points = 0
            user_name = ''

        try:
            user_region = user_info['payload']['games']['csgo']['region']
        except KeyError:
            user_region = ''

        useful_data = {
            'name': user_info['payload']['nickname'],
            'steam_id': user_info['payload']['steam_id'],
            'steam_hours': steam_hours,
            'faceit_id': user_info['payload']['guid'],
            'facebook_id': (user_info['payload']['facebook'].get('fb_uid', '')
                            if user_info['payload'].get('facebook', {}) else ''),
            'country': user_info['payload']['country'],
            'region': user_region,
            'league_name': user_name,
            'league_rank': user_rank,
            'league_points': user_points,
        }
        res = self._model.save_new_user(useful_data)

        if not res:
            return False

        self._model.save_friends(user_info['payload']['guid'], user_info['payload']['friends_ids'])
        return True

    @classmethod
    def get_steam_hours(cls, steam_id64: str) -> str:
        """Parse Steam API and get played steam hours"""
        if not steam_id64:
            return ''
        res = requests.get('http://steamcommunity.com/profiles/{:s}?xml=1'.format(steam_id64))
        root = et.fromstring(res.content)
        for game in root.iter('mostPlayedGame'):
            if game[0].text == 'Counter-Strike: Global Offensive':
                for game_time in game.iter('hoursOnRecord'):
                    return game_time.text
        return ''

    @classmethod
    def get_info(cls, name: str) -> dict:
        """Get info about user"""
        return Crawler.get_api_json("https://api.faceit.com/api/nicknames/{:s}".format(name))

    @classmethod
    def get_league_info(cls, user_id: str) -> dict:
        """ Get user info about league"""
        url = "https://api.faceit.com/api/users/{:s}/games/csgo/league?region={:s}".format(user_id, config.region)
        return Crawler.get_api_json(url)


class Match(Crawler):
    """Get all data about match"""

    def __init__(self):
        super().__init__()
        self._model = model.MatchModel()
        self._list_model = model.UserListModel()
        self._new_users = []
        self._matches = []
        self._user_id = ''

    def run(self):
        """Parse match list"""

        for match in self._matches:
            self.parse_match_history(match)

    def parse_match_history(self, match_id: str):
        """Parse data from match"""
        url = 'https://api.faceit.com/api/matches/{:s}?withStats=true'.format(match_id)
        data = Crawler.get_api_json(url)

        # catch api errors
        try:
            if data['payload'].get('winner'):
                result = int(self._user_id in [user['guid'] for user in data['payload'][data['payload']['winner']]])
            else:
                result = -1
        except KeyError as error:
            with open('errors.log', 'w+') as log:
                log.write("{}:\n\t{}\n".format(error, str(requests.get(url).content)))
                raise KeyError

        match_history = {
            'player_id': self._user_id,
            'match_id': match_id,
            'result': result,
            'match_date': data['payload']['created_at'],
            'mode': config.league_type,
            'score': self.get_score(match_id),
            # 'score': '{:d}:{:d}'.format(int(data['payload']['score1']), int(data['payload']['score2'])),
            'demo': data['payload']['external_matches'][0]['stats'].get('demo_file_url', '')
        }

        # get new users from teams
        self._new_users += [user['nickname'] for user in data['payload']['faction1']]
        self._new_users += [user['nickname'] for user in data['payload']['faction2']]

        # save to db
        self._model.dave_match_history(match_history)

    @classmethod
    def get_score(cls, match_id: str) -> str:
        """Get match result"""
        data = Crawler.get_api_json('https://api.faceit.com/stats/api/v1/stats/matches/{:s}'.format(match_id))
        return data[0]['i18']

    def parse_matches_information(self, user: str):
        """Parse data from user match list"""

        url = 'https://api.faceit.com/stats/api/v1/stats/time/users/{}/games/csgo?page=0&size={}'.format(
                user, config.last_matches_quantity)
        data = Crawler.get_api_json(url)

        self._user_id = user
        match_information = []

        # parse each user match
        for match in data:
            info = {
                'match_id': match['matchId'],
                'team': match['i5'],
                'player': self._user_id,
                'kills': match['i6'],
                'assists': match['i7'],
                'deaths': match['i8'],
                'KR': match['c3'],
                'KD': match['c2'],
                'headshot': match['i13'],
                'headshot_percent': match['c4'],
                'MVP': match['i9'],
            }
            match_information.append(info)
            self._matches.append(match['matchId'])

        # save to db
        if match_information:
            self._model.save_match_info(match_information)

    def set_users(self):
        """Set new users from matches"""

        for user in self._new_users:
            self._list_model.insert_user(user)
