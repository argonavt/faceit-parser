from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class Driver(webdriver.PhantomJS):
    """ PhantomJS driver

        Keep setting and browser control method
    """

    def __init__(self):
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        # disable loading images
        dcap['phantomjs.page.settings.loadImages'] = False

        # init Phantom driver
        super().__init__(desired_capabilities=dcap)
        self._remove_files_from_loading()

        self._driver_wait_time = 10
        self._screen = './browser.png'

    def __del__(self):
        self.close()

    def _remove_files_from_loading(self):
        """Cancel loading files"""
        self.command_executor._commands['executePhantomScript'] = ('POST', '/session/$sessionId/phantom/execute')
        self.execute('executePhantomScript', {'script': '''
            this.onResourceRequested = function(requestData, request) {
                files = [
                    '.css', '.ttf', 'www.google.com', 'googleadservices.com', 'google-analytics.com',
                    'googletagservices.com', 'googlesyndication.com', 'facebook.com', 'facebook.net', 'jquery',
                    'cdn.optimizely.com', '/locale-en.json', 'cloudfront.net', 'chartbeat', 'mxpnl.com',
                    'newrelic.com', 'analytics.js',  'amazonaws.com', 'pusherapp.com', '24techteam.co.uk',
                    'newrelic-90c44e02a6.js', 'rawgit.com', 'pingdom.net', 'gpt.js', 'container.html', '.woff2',
                    'twitch.tv', 'api/streamings', 'tournaments/upcoming', '/presence/count', '/api/time',
                    'doubleclick.net', '0914.global.ssl.fastly.net', '/proxy.html', 'mixpanel.com', 'keen.io',
                    'optimizely.com', 'jtvnw.net', 'pusher.com', 'nr-data.net', 'P5DLcu0KGJB.js?version=42',
                ]
                if(RegExp(files.join('|'),'i').test(requestData.url)) {
                    request.abort();
                }
            }
        ''', 'args': []})

    def load_page(self, url: str):
        """Load web page"""
        self.get(url)

    def select_nodes(self, selector: str, mod='css') -> list:
        """Get node of DOM"""

        if mod == 'css':
            select = self.find_elements_by_css_selector
            by = By.CSS_SELECTOR

        WebDriverWait(self, self._driver_wait_time).until(
            ec.presence_of_element_located((by, selector))
        )

        return select(selector)

    def save_screen(self):
        """Save screen of last rendered page"""
        self.save_screenshot(self._screen)
