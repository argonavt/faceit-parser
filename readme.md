# Faceit players scraper

Parse [faceit](https://www.faceit.com/) site and get all data about players and their matches.

### Usage:
    Configure file config.py and run main.py
    
### Requirements:
    PhantomJS, selenium, pymysql, requests